# -*- coding: utf-8 -*-
"""
// Copyright (C) 2015
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2 as
// published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

Automatic Leakage Detection Application (ALDA)
author: Rodolfo Alvarado Montero
version 1.4 - updated May 2018
first created on November 2017
approach based on sequential data assimilation
"""

import numpy as np
import pandas as pd
import scipy.stats as ss
import time
import matplotlib.pyplot as plt

#parameters
n = 1.852 # Hazen-Williams exponent
err_NR = 1e-8 # error of iterations of Newton-Raphson method
detect = True


#id,elevation,lateral,boolean leakages (0 for no leakage, 1 for leakage)
nodes = np.array([
        [1,0.0,0.0,0],
        [2,0.0,0.0,1],
        [3,0.0,0.0,0],
        [4,0.0,0.0,0],
        [5,0.0,0.0,0],
        [6,0.0,0.005,0],
        ])

#id,Nup,Ndw,r
connectors = np.array([
        [0,0,1,15651.8],
        [1,1,2,15651.8],
        [2,2,3,15651.8],
        [3,1,4,15651.8],
        [4,2,5,15651.8],
        [5,3,6,15651.8],
        [6,4,5,15651.8],
        [7,5,6,15651.8],
        ])

#id, elevation, pressure
reservoirs = np.array([
        [0,0,30],
        ])

#pressure observations
p_obs_value = [27.63,28.10]
p_obs_index = [3,4]       #pressure node ids
# p_obs_value = [28.4,27.79,27.63,28.10,27.79,27.47]
# p_obs_index = [1,2,3,4,5,6]       #pressure node ids

#discharge observations
q_obs_value = []
q_obs_index = []       #discharge ids
# q_obs_value = [0.00415,0.00204,0.00285,0.00011,0.00204,0.00285,0.00295]
# q_obs_index = [1,2,3,4,5,6,7]       #discharge ids

#initial guess of leakages
initial_guess = 0.005

#basic variables
Nodes = np.size(nodes,0)
Connectors = np.size(connectors,0)
Reservoirs = np.size(reservoirs,0)
dfNodes = pd.DataFrame(nodes)
dfConnectors = pd.DataFrame(connectors)
dfReservoirs = pd.DataFrame(reservoirs)
dfJunctions = pd.concat([dfNodes.loc[:,0:1],dfReservoirs.loc[:,0:1]], ignore_index = True)

# preprocessing of elevations to be added to the connectors dataframe
tempUp = np.empty((Connectors,1))
tempDw = np.empty((Connectors,1))
for i in range(Connectors):
    tempUp[i] = dfJunctions.loc[dfJunctions[0] == connectors[i,1], 1]
    tempDw[i] = dfJunctions.loc[dfJunctions[0] == connectors[i,2], 1]
dfConnectors[4] = tempUp  #careful: this modifies the array connectors!
dfConnectors[5] = tempDw  #careful: this modifies the array connectors!

def simulate(leakages):

    # initial values - simulation can be sensitive to initial guess!
    pressure = np.ones((Nodes+Reservoirs,1))*1.0
    discharge = np.ones((Connectors,1))*1.0
    sol = np.r_[pressure, discharge]
    F = np.ones((Nodes+Reservoirs+Connectors,1)) #made of ones so that it can initialize
    dF = np.zeros((Nodes+Reservoirs+Connectors,Nodes+Reservoirs+Connectors)) # [dF/dP, dF/dQ]
    
    while np.max(F) > err_NR:
        eq = 0
        # energy
        for j in range(Connectors):
            F[eq] = pressure[int(connectors[j,1])] + dfConnectors.loc[j,4] - \
                    pressure[int(connectors[j,2])] - dfConnectors.loc[j,5] - \
                    connectors[j,3]*abs(discharge[int(connectors[j,0])])**(n-1)*discharge[int(connectors[j,0])]
            dF[eq,int(connectors[j,1])] = 1.0
            dF[eq,int(connectors[j,2])] = -1.0
            dF[eq,int(Nodes)+int(Reservoirs)+int(connectors[j,0])] = -1.0*n*connectors[j,3]*abs(discharge[int(connectors[j,0])])**(n-2)*discharge[int(connectors[j,0])]
            eq = eq + 1
    
        #continuity
        i_leakages = 0
        for j in range(Nodes):
            matchUp = dfConnectors.loc[dfConnectors[1] == nodes[j,0]]
            matchDw = dfConnectors.loc[dfConnectors[2] == nodes[j,0]]
            SumUp = 0.0
            SumDw = 0.0
            for k in matchUp[0]:                
                SumUp = SumUp + discharge[int(k)]    #check if this makes sense!
                dF[eq,int(Nodes)+int(Reservoirs)+int(k)] = -1.0
            for k in matchDw[0]:
                SumDw = SumDw + discharge[int(k)]
                dF[eq,int(Nodes)+int(Reservoirs)+int(k)] = 1.0
            if nodes[j,3] == 1:
                F[eq] = SumDw - SumUp - nodes[j,2] - leakages[i_leakages]
                i_leakages = i_leakages + 1
            elif nodes[j,3] == 0:
                F[eq] = SumDw - SumUp - nodes[j,2]
            eq = eq + 1

        #reservoirs
        for j in range(Reservoirs):
            F[eq] = pressure[reservoirs[j,0]] - reservoirs[j,2]
            dF[eq,reservoirs[j,0]] = 1
            eq = eq + 1
    
        #solve the system
        sol = sol - np.dot(np.linalg.inv(dF),F)
        pressure = sol[0:Nodes+Reservoirs]
        discharge = sol[Nodes+Reservoirs:]
    return [pressure, discharge]

def tic():
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()

def toc():
    if 'startTime_for_tictoc' in globals():
        print ("Elapsed time is " + str(time.time() - startTime_for_tictoc) + " seconds.")
    else:
        print ("Toc: start time not set")

# steps for data assimilation approach
# introduce noise to the forcing (leakages with a given uncertainty)
# obtain the ensemble of pressure and discharges in the system
# define the observation operator
# introduce uncertainty to observation
# run the EnKF and update the simulated values and the forcings based on observation
# introduce noise to the updated forcing and rerun

# n states, m observations, l ensemble members
# A (analysis/update step) = [n x l], F (forecasts/prior)= [n x l]
# K (Kalman gain) = [n x m], Z (observation vector) = [m x l]
# H (observation operator) = [m x n], P (error covariance) = [n x n]
# R (observation error covariance) = [m x m]

def set_Normal_limited_noise(mu,sigma,low,high,number_ensembles):   #SHOULD SIGMA ALWAYS BE POSITITVE?
    #noise = np.random.normal(mu,sigma,number_ensembles) #PREVIOUS IMPLEMENTATION
    noise_temp = ss.truncnorm((low - mu) / sigma, (high - mu) / sigma, loc=mu, scale=sigma)
    noise = noise_temp.rvs(number_ensembles)
    return (noise)

def enKF(forecast,obs_operator,observation):
    P = np.cov(forecast)
    R = np.cov(observation)
    num = np.dot(P,np.transpose(obs_operator))
    den = np.dot(obs_operator,num) + R
    temp = np.linalg.inv(den)
    gain = np.dot(num,temp)
    A = forecast + np.dot(gain,(observation-np.dot(obs_operator,forecast)))
    return A

l_size = np.count_nonzero(nodes[:,3] == 1) # number of leakages
df = pd.DataFrame()

if detect:
    tic()

    leakages = np.ones((l_size,1))*initial_guess #initial guess
    ensemble_size = 50*l_size
    H = np.zeros((len(p_obs_value)+len(q_obs_value),l_size+Reservoirs+Nodes+Connectors))
    for i in range(len(p_obs_value)):
        H[i,l_size+p_obs_index[i]] = 1.0    #leakages,pressure,discharge

    for i in range(len(q_obs_value)):
        H[i+len(p_obs_value),l_size+Reservoirs+Nodes+q_obs_index[i]] = 1.0    #leakages,pressure,discharge

    leakages_array = np.empty((ensemble_size,l_size))
    observations_array = np.empty((ensemble_size,len(p_obs_value)+len(q_obs_value)))
    forecast_array = np.empty((l_size+Reservoirs+Nodes+Connectors,ensemble_size))

    #perturbations to observations
    for i in range(len(p_obs_value)):
        observations_array[:,i] = set_Normal_limited_noise(p_obs_value[i],p_obs_value[i]*0.005,p_obs_value[i]*0.9,p_obs_value[i]*1.1,ensemble_size) #assumes very low uncertainty of observations!
    for i in range(len(q_obs_value)):
        observations_array[:,len(p_obs_value)+i] = set_Normal_limited_noise(q_obs_value[i],q_obs_value[i]*0.005,q_obs_value[i]*0.9,q_obs_value[i]*1.1,ensemble_size) #assumes very low uncertainty of observations!

    B = np.empty((20,ensemble_size))  #initialize result matrix for box plot

    for k in range(20):     #assumes 10 observations with continuous equal measurements
        # perturbations to best estimate of leakages
        for i in range(l_size):
            #HOW CAN WE PERTURBE THE MODEL IN ORDER TO DETECT ZERO LEAKAGES IN A ROBUST WAY?
            leakages_array[:, i] = set_Normal_limited_noise(leakages[i], abs(leakages[i]) * 0.1, min(leakages[i] * 0.5,leakages[i] * 1.5), max(leakages[i] * 0.5,leakages[i] * 1.5),ensemble_size)

        #simulation ensemble
        for i in range(ensemble_size):
            [P, Q] = simulate(leakages_array[i,:])
            L = leakages_array[i,:]
            forecast_array[:,i] = np.concatenate((L,P[:,0],Q[:,0]))
    
        #update run
        A = enKF(forecast_array,H,np.transpose(observations_array))
        leakages = np.mean(A[:l_size,:],axis=1)

        #save results
        B[k,:] = np.transpose(leakages_array[:,0])
        # B[k,:] = A[0,:]

        #testing
        # print ('iteration = ',k)
        # plt.figure(1)
        # plt.plot(A[0,:])    #change it according to observation node
        # plt.ylabel('leakage on N2')
        # plt.xlabel('ensemble member')
        # plt.axhline(y=0.002,color='r',linestyle=':')
        # plt.ylim((0.00,0.005))
        # plt.show()

    leakages = np.mean(leakages_array,0) #CHECK!
    toc()

    print ('************* ENDED *************')
    print ('leakages = %s' % (leakages))
    [pressure, discharge] = simulate(leakages)
    print ('pressure = %s' %(pressure))
    print ('discharge = %s' %(discharge))

    plt.boxplot(np.transpose(B))
    plt.ylabel('leakage on N2')
    plt.xlabel('Sequential iterations')
    plt.axhline(y=0.002,color='r',linestyle=':')
    plt.ylim((0.00,0.008))
    plt.show()

else:
    print ('************* ENDED *************')
    leakages = np.zeros((l_size,1))
    [pressure, discharge] = simulate(leakages)
    print ('pressure = %s' %(pressure))
    print ('discharge = %s' %(discharge))
