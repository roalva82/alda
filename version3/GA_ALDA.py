import numpy as np

def GA(func, lb, ub, pop_size, abs_mut_change = 0.4, abs_mut_change_p = 0.6, rel_mut_change = 0.4, max_mut = 0.1, maxiter = 100, min_step = 1, min_func = 1e-8, minfunclim = 1e-2):
    assert len(lb) == len(ub), 'Lower- and upper-bounds must be the same length'
    assert hasattr(func, '__call__'), 'Invalid function handle'
    lb = np.array(lb)
    ub = np.array(ub)
    assert np.all(ub > lb), 'All upper-bound values must be greater than lower-bound values'
    obj = lambda x: func(x)
    D = len(lb) #number of gens
    x = np.random.rand(pop_size, D)
    f = np.zeros(pop_size) #function values
    best = [] # best individual
    best_val = 0.0 # value of the best individual
    #creating initial population

    for i in range(pop_size):
        x[i, :] = np.log10(lb + x[i, :] * (ub - lb))
        f[i] = 1.0 / obj(10 ** x[i, :])
        if(f[i] > best_val):
            best_val = f[i]
            best = x[i]
    prev_pop = np.copy(x)
    np.append(prev_pop, x)
    f_prev = np.copy(f)
    np.append(f_prev, best_val)

    repro_change2 = [y / sum(f_prev) for y in f_prev]
    repro_change = np.cumsum(repro_change2)
    it = 0
    par = np.zeros(2)
    status = False
    while it < maxiter:
        print("Iteration " + str(it))
        for i in range(pop_size):
            #get parents
            par[0] = next(x for x, val in enumerate(repro_change)if val > np.random.rand())
            par[1] = next(x for x, val in enumerate(repro_change)if val > np.random.rand())
            #create child
            for j in range(D):
                x[i,j] = prev_pop[int(par[np.random.randint(0,1)]),j]
            #mutation
            abs_mut = np.random.rand()
            if (par[0] == par[1]):
                if (abs_mut < abs_mut_change_p):
                    x[i, :] = np.random.rand( D)
                    x[i, :] = np.log10(lb + x[i, :] * (ub - lb))
            else:
                if(abs_mut < abs_mut_change):
                    x[i, :] = np.random.rand( D)
                    x[i, :] = np.log10(lb + x[i, :] * (ub - lb))
                #absolute mutation
            rel_mut = np.random.rand()

            if(rel_mut < rel_mut_change):
                #relative mutation
                x[i, :] = (1 + np.random.rand(2) - 0.5) * 2 * max_mut * x[i,:]

            #run simulation
            f[i] = 1.0 / obj(10 ** x[i, :])
            if(f[i] > best_val):
                best_val = f[i]
                best = x[i]
            if 1.0 / best_val <= minfunclim:
                print('Stopping search: Swarm best objective less than {:}'.format(minfunclim))
                status = True
                tmp = x[i, :].copy()
                return tmp, best_val, status

        prev_pop = np.copy(x)
        np.append(prev_pop, x)
        f_prev = np.copy(f)
        print("Best value is " + str(1.0 / best_val))
        np.append(f_prev, best_val)
        repro_change2 = [y / sum(f_prev) for y in f_prev]
        repro_change = np.cumsum(repro_change2)
        it += 1
    print('Stopping search: maximum iterations reached --> {:}'.format(maxiter))
    return best, best_val, status

