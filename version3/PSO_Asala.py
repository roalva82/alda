import numpy as np

from multiprocessing import Process
def pso(func, lb, ub, swarmsize=100, omega=0.5, phip=0.5, phig=0.5, maxiter=100,
        minstep=1, minfunc=1e-8, minfunclim = 10):
    """
    Perform a particle swarm optimization (PSO)

    Parameters
    ==========
    func : function
        The function to be minimized
    lb : array
        The lower bounds of the design variable(s)
    ub : array
        The upper bounds of the design variable(s)

    Optional
    ========
    swarmsize : int
        The number of particles in the swarm (Default: 100)
    omega : scalar
        Particle velocity scaling factor (Default: 0.5)
    phip : scalar
        Scaling factor to search away from the particle's best known position
        (Default: 0.5)
    phig : scalar
        Scaling factor to search away from the swarm's best known position
        (Default: 0.5)
    maxiter : int
        The maximum number of iterations for the swarm to search (Default: 100)
    minstep : scalar
        The minimum stepsize of swarm's best position before the search
        terminates (Default: 1e-8)
    minfunc : scalar
        The minimum change of swarm's best objective value before the search
        terminates (Default: 1e-8)
    minfunclim: scalar
        This is the upper limit for satisfying the objective function goal

    Returns
    =======
    g : array
        The swarm's best known position (optimal design)
    f : scalar
        The objective value at ``g``
    status: boolean
        It returns True once the optimization conditions are satisfied,
        Otherwise, it returns False which keeps the process going


    """
    status = False
    assert len(lb) == len(ub), 'Lower- and upper-bounds must be the same length'
    assert hasattr(func, '__call__'), 'Invalid function handle'
    lb = np.array(lb)
    ub = np.array(ub)
    assert np.all(ub > lb), 'All upper-bound values must be greater than lower-bound values'

    vhigh = np.abs(ub - lb)
    vlow = -vhigh

    # Check for constraint function(s) #########################################
    obj = lambda x: func(x)


    # Initialize the particle swarm ############################################
    S = swarmsize
    D = len(lb)  # the number of dimensions each particle has
    x = np.random.rand(S, D)  # particle positions
    v = np.zeros_like(x)  # particle velocities
    p = np.zeros_like(x)  # best particle positions
    fp = np.zeros(S)  # best particle function values
    g = []  # best swarm position
    fg = 1e100  # artificial best swarm position starting value




    for i in range(S):


        # Initialize the particle's position
        x[i, :] =lb + x[i, :] * (ub - lb)


        # Initialize the particle's best known position
        p[i, :] = x[i, :]

        # Calculate the objective's value at the current particle's
        fp[i] = obj(p[i, :])

        #print('result is ' + str(fp[i]))
        # At the start, there may not be any feasible starting point, so just
        # give it a temporary "best" point since it's likely to change
        if i == 0:
            g = p[0, :].copy()

        # If the current particle's position is better than the swarm's,
        # update the best swarm position
        if fp[i] < fg :
            fg = fp[i]
            g = p[i, :].copy()

        # Initialize the particle's velocity
        v[i, :] = vlow + np.random.rand(D) * (vhigh - vlow)

    # Iterate until termination criterion met ##################################
    it = 0
    min_val =[min(fp)]
    print(min_val)
    while it <= maxiter:
        print("Iteration " + str(it))
        rp = np.random.rand(S, D)
        rg = np.random.rand(S, D)

        for i in range(S):

            # Update the particle's velocity
            v[i, :] = omega * v[i, :] + phip * rp[i, :] * (p[i, :] - x[i, :]) + \
                      phig * rg[i, :] * (g - x[i, :])

            # Update the particle's position, correcting lower and upper bound
            # violations, then update the objective function value
            x[i, :] = x[i, :] + v[i, :]
            mark1 = x[i, :] < lb
            mark2 = x[i, :] > ub
            x[i, mark1] = lb[mark1]
            x[i, mark2] = ub[mark2]

            fx = obj(x[i, :])


            #print('result is ' + str (fx))
            if fx <= minfunclim:
                print('Stopping search: Swarm best objective less than {:}'.format(minfunclim))
                status = True
                tmp = x[i, :].copy()
                return tmp, fx, status

            # Compare particle's best position (if constraints are satisfied)
            if fx < fp[i]:
                p[i, :] = x[i, :].copy()
                fp[i] = fx


                # Compare swarm's best position to current particle's position
                # (Can only get here if constraints are satisfied)
                if fx < fg:

                    tmp = x[i, :].copy()
                    stepsize = np.sqrt(np.sum((g - tmp) ** 2))
                    if np.abs(fg - fx) <= minfunc:
                        print('Stopping search: Swarm best objective change less than {:}'.format(minfunc))
                        status = True
                        return tmp, fx, status
                    elif stepsize <= minstep:
                        print('Stopping search: Swarm best position change less than {:}'.format(minstep))
                        status = True
                        return tmp, fx, status
                    else:
                        g = x[i, :].copy()
                        fg = fx


        it += 1
        min_val.append(min(fp))
        print(min_val)

    print('Stopping search: maximum iterations reached --> {:}'.format(maxiter))
    return g, fg, status

