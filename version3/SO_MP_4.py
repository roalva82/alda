"""
    ALDA - Automatic Leakage Detection Application
    This ALDA package was writen by Asala Mahajna under the supervision of Sam van der Zwan and Rodolfo Alvarado Montero
    as part of her master's thesis project at Delatres.
    This package utilizes EPANETTOOLS package
    Also, this package uses the bugged byswarm package(this package has been greatly manually edited)
 """

import multiprocessing
from epanettools.epanettools import EPANetSimulation, Node, Link
from epanettools.examples import simple
import tempfile, os
import numpy as np
import time
import math
import csv
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from PSO_Asala import pso
from GA_ALDA import GA
from shutil import copyfile


def tic():
    global startTime_for_tictoc
    startTime_for_tictoc = time.time()


def toc():
    if 'startTime_for_tictoc' in globals():
        return time.time() - startTime_for_tictoc
        print("Elapsed time is " + str(time.time() - startTime_for_tictoc) + " seconds.")
    else:
        print("Toc: start time not set")
class epanet_object:
    def __init__(self,file, pres_meas, flow_meas, leak_location, leaks):
        self.index = 0
        self.file = file
        es = EPANetSimulation(self.file)
        self.pres_meas = []
        self.flow_meas = []
        self.leak_loc = []
        self.base_dem = []
        node_list = [es.network.nodes[x].id for x in list(es.network.nodes)]
        pipe_list = [es.network.links[x].id for x in list(es.network.links)]
        d = Node.value_type['EN_BASEDEMAND']
        for meas_point in pres_meas:
            self.pres_meas.append(node_list.index(meas_point) + 1)
        for meas_point in flow_meas:
            self.flow_meas.append(pipe_list.index(meas_point) + 1)
        for leak in leak_location:
            self.leak_loc.append(node_list.index(leak) + 1)
            self.base_dem.append( es.ENgetnodevalue(self.leak_loc[-1], d)[1])
        self.set_leakage(leaks, es)
        run = self.run_simulation(es)
        [self.head_measurement, self.flow_measurement] = self.get_results(run)
        es.clean()

    def set_leakage(self,leaks, es):
        d = Node.value_type['EN_BASEDEMAND']
        for leak_dem, leak, leak_loc in zip(self.base_dem, leaks, self.leak_loc):
            leak_value = leak + leak_dem
            r = es.ENsetnodevalue(leak_loc, d, float(leak_value))

    def run_simulation(self,es):
        cwd = os.getcwd()
        f = os.path.join(cwd +"temp" + str(self.index) + ".inp")
        es.ENsaveinpfile(f)
        run = EPANetSimulation(f)
        run.run()
        run.clean()
        self.index += 1
        return run

    def get_results(self,run):
        p = Node.value_type['EN_HEAD']
        d = Link.value_type['EN_FLOW']
        #heads are taken at the nodes
        head_result = []
        for item in self.pres_meas:
            head_result.append(run.network.nodes[item].results[p])
        #flows are taken from the pipes
        flow_result = []
        for item in self.flow_meas:
            flow_result.append(run.network.links[item].results[d])
        return head_result, flow_result

    def get_dif(self,leaks, queue = 'None'):
        #print("leakage is " + str(leaks))
        #copy file
        es = EPANetSimulation(self.file)
        self.set_leakage(leaks,es)
        run = self.run_simulation(es)
        [head_result, flow_result] = self.get_results(run)
        dif_head = []
        for res, meas in zip(head_result, self.head_measurement ):
            dif = 0
            for val1, val2 in zip(res,meas):
                dif += (val1 - val2) ** 2
            dif /= (max(meas) - min(meas))
            dif /= float(len(meas))
            dif_head.append(dif)
        difflow = 0
        if(self.flow_meas):
            for res, meas in zip(flow_result, self.flow_measurement ):
                for val1, val2 in zip(res,meas):
                    difflow += (val1 - val2) ** 2
            difflow /=  max(max(self.flow_measurement))
            difflow /= float(len(flow_result) * len(flow_result[0]) )
        es.clean()
        queue.put(math.sqrt(sum(dif_head) + difflow))
        return math.sqrt(sum(dif_head) + difflow)

    def proces(self,leaks):
        q = multiprocessing.Queue()
        processes = []
        process = multiprocessing.Process(target=self.get_dif, args=( (leaks),q, ))
        process.start()
        ret = q.get()  # will block
        process.join()
        return ret

def writedat(filename,leak_location,times, res,  opt, xprecision=5):
    with open(filename,  'w', newline='') as csv_file:
        fieldnames = ['Time']
        for loc in leak_location:
            fieldnames.append(loc)
        fieldnames.append('result')
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for i in range(len(times)):
            row = {fieldnames[0]:times[i]}
            for field, val in zip(fieldnames[1:-1], res[i]):
                row[field] = val
            row[fieldnames[-1]] = opt[i]
            writer.writerow(row)


if __name__ == '__main__':

    # Calling Simulation function for outputting measure hydraulic data
    pres_meas = ['13', '10','31']
    flow_measure = []
    leak_location = ['12', '31']
    leak = [200, 0]
    file = os.path.join(os.path.dirname(simple.__file__), 'Net1.inp')  # open an example
    epanetobject = epanet_object(file, pres_meas, flow_measure,leak_location, leak)
    lb = np.zeros(len(leak_location), dtype=np.float)
    ub = np.zeros(len(leak_location), dtype=np.float)
    len(lb) == len(ub)
    for i in range(len(leak_location)):
         lb[i] = float(0)
         ub[i] = float(400)
#
    times = []
    res = []
    res2 = []
    opt = []
    for i in range(1):
         tic()
# #        xopt, fopt, status = GA(epanetobject.proces, lb, ub, 20)
         xopt, fopt, status = pso(epanetobject.proces, lb, ub,
                      swarmsize=20, omega=1.0, phip=0.5, phig=0.5, maxiter=100, minstep=0.001,
                      minfunc=1e-8, minfunclim = 0.4)
         for i in range(len(leak_location)):
             print('The leakage at node ' + leak_location[i]+ '= %.3f' % (xopt[i]))

         print('The minimum value = %.3f' % (fopt))
         res.append(xopt)
         opt.append(fopt)
         times.append(toc())
         print(times[-1])
    writedat('test3.csv',leak_location,times, res,  opt)
    print("Average time used " + str(sum(times) / len(times)))
    print("Average result 1 " + str(sum(res) / len(times)))
    print("Average result 2 " + str(sum(res2) / len(times)))








