import os, pprint
pp = pprint.PrettyPrinter()  # we'll use this later.
from epanettools.epanettools import EPANetSimulation, Node, Link, Network, Nodes, Links, Patterns, Pattern, \
    Controls, Control  # import all elements needed
from epanettools.examples import simple  # this is just to get the path of standard examples
file = os.path.join(os.path.dirname(simple.__file__), 'Net1.inp')  # open an example
es = EPANetSimulation(file)
import os
import math
from epanettools import epanet2 as et
from epanettools.examples import simple
file = os.path.join(os.path.dirname(simple.__file__), 'Net1.inp')
ret = et.ENopen(file, "Net1.rpt", "")
import tempfile, os
import numpy as np

from Simulation import obs_result

(demand_obs, head_obs, pressure_obs) = obs_result()

index = 0
e = [0] * 500
f = [0] * 500
def sim(dt):
    tot = 0
    dta = np.float(dt[0])
    dtb = np.float(dt[1])
    dtc = np.float(dt[2])

    global index,  e, f, demand_obs, head_obs, pressure_obs
    d = Node.value_type['EN_BASEDEMAND']
    es.ENgetnodevalue(6, d)[1]  # low level interface
    es.network.nodes[6].results[d]  # new interface
    es.ENgetnodevalue(2, d)[1]  # low level interface
    es.network.nodes[2].results[d]

    es.ENgetnodevalue(9, d)[1]  # low level interface
    es.network.nodes[9].results[d]
    r = es.ENsetnodevalue(6, d, float(dta))  # now let's change values - link
    rr = es.ENsetnodevalue(2, d, float(dtb))
    rrr = es.ENsetnodevalue(9, d, float(dtc))
    f[index] = os.path.join(tempfile.gettempdir(), "temp.inp")
    es.ENsaveinpfile(f[index])  # save the changed file
    r = es.ENsaveinpfile(f[index])
    #print("Current temp directory:", tempfile.gettempdir())
    e[index] = EPANetSimulation(f[index])
    e[index].run()
    p = Node.value_type['EN_PRESSURE']
    h = Node.value_type['EN_HEAD']
    de = Node.value_type['EN_DEMAND']
    ret, Nnodes = et.ENgetcount(et.EN_NODECOUNT)
    s = (9, 24)
    demand_sim = np.zeros(s)
    head_sim = np.zeros(s)
    pressure_sim = np.zeros(s)

    for j in range(0, 24):
        for i in range(0, Nnodes - 2):
            demand_sim[i][j] = "%.3f" % e[index].network.nodes[i + 1].results[de][j + 1]
            pressure_sim[i][j] = "%.3f" % e[index].network.nodes[i + 1].results[p][j + 1]
            head_sim[i][j] = "%.3f" % e[index].network.nodes[i + 1].results[h][j + 1]

    for j in range(0, 24):
        for i in range(0, Nnodes - 2):
            tot += math.fabs(np.float(demand_obs[i][j]) - np.float(demand_sim[i][j])) + math.fabs(
                np.float(pressure_obs[i][j]) - np.float(pressure_sim[i][j])) + math.fabs(
                np.float(head_obs[i][j]) - np.float(head_sim[i][j]))

    index += 1
    #print(index)
    #print('The new Objective F value {:} '.format(tot))
    #print(' for xopt {:}'.format(dt))
    return tot